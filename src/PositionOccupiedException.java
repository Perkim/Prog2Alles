

/**
 * Created by Andreas on 04.07.2016.
 */
public class PositionOccupiedException extends Exception {
    private static final String POSITION_OCCUPIED_MSG = "Position im Array ist bereits belegt!";

    public PositionOccupiedException() {
        super(POSITION_OCCUPIED_MSG);
    }

}
