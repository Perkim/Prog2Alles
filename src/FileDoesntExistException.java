
/**
 * Exception-Klasse, falls das File nicht existiert
 * 
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan 
 * @version 130616
 */
import java.io.*;

public class FileDoesntExistException extends Exception
{
    private static final String FILE_INEXISTENT_MSG = "Die eingegebene Datei existiert nicht!"; 
    
    public FileDoesntExistException() {
        super(FILE_INEXISTENT_MSG);
    }
}
