
/**
 * Exception-Klasse, falls das File nicht lesbar ist
 * 
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan 
 * @version 130616
 */
public class FileNotReadableException extends Exception
{
    private static final String FILE_UNREADABLE_MSG = "Die eingegebene Datei ist nicht lesbar! Bitte einlesbare Datei eingeben!";
    
    public FileNotReadableException() {
        super(FILE_UNREADABLE_MSG); 
    }
}
