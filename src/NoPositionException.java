/**
 * Created by Andreas on 04.07.2016.
 */
public class NoPositionException  extends Exception{

    private static  final String POSITION_NOT_YET_USED_MSG = "Position im Array ist noch nicht belegt!";

    public NoPositionException() {
        super(POSITION_NOT_YET_USED_MSG);
    }
}
