
/**
 * Nutzdaten für den 2-3-4 Baum
 *
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan
 * @version 28.06.16
 */

import java.io.*;

public class DataItem {

    public String dData;


    /**
     * Konstruktor für ein Element des Baumes
     * @param d     Key des Elements
     */
    public DataItem(String d) {
        dData = d;
    }

    /**
     * Getter für das Datenelement | Gleichzeitig toString-Methode
     * @return  Name des Datenelements
     */
    public String getData() {
        return dData;
    }

    /**
     * Setter für das Datenelement
     * @param newData   neuer Name des Datenelements
     */
    public void setData(String newData) {
        this.dData = newData;
    }


    /**
     * Methode zum Printen des Elements
     */
    public void displayItem() {
        System.out.print("/" + dData);
    }
}
