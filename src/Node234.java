
/**
 * Knoten für 2-3-4 Baum und deren Funktionen
 *
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan
 * @version 28.06.16
 */



public class Node234
{
    private static final int MAX = 4;
    private int elementCounter;
    private Node234 parent;
    private Node234 childArray[] = new Node234[MAX];
    private DataItem dItemArray[] = new DataItem[MAX - 1];


    /**
     * Method connectChildToNode
     * Verbindet Kindknoten an den Knoten
     * @param childPos Position des Kindknoten im Array
     * @param child Kindknoten
     */
    public void connectChildToNode(int childPos, Node234 child) throws PositionOccupiedException {
        if(childArray[childPos] == null) {
            childArray[childPos] = child;
            if(child != null) {
                child.parent = this;
            }
        } else {
            throw new PositionOccupiedException();
        }
    }

    /**
     * Method disconnectChildFromNode
     * Entfernt einen Kindknoten von einem Knoten
     * @param childPos Position des Kindknoten im Array
     * @return Der befreite Knoten
     */
    public Node234 disconnectChildFromNode(int childPos) throws NoPositionException {
        if(childArray[childPos] != null) {
            Node234 tempNode = childArray[childPos];
            childArray[childPos] = null;
            return tempNode;
        } else {
            throw new NoPositionException();
        }
    }

    /**
     * Durchsucht den Knoten, ob ein gesuchtes Datenelement enthalten ist
     * @param key das gesuchte Element
     * @return Die Stelle, an dem es sich befindet; Im Fehlerfall -1
     */
    public int findItem(String key) {
        for(int i = 0; i < MAX - 1; i++) {
            if(dItemArray[i] == null) {
                break;
            } else if(dItemArray[i].dData.equals(key)) {
                return i;
            }
        }
        return -1;
    }


    /**
     * Fügt dem Knoten ein neues Datenelement hinzu
     * @param newData das neue Datenelement
     * @return die Stelle, an dem das Element eingefügt wurde
     */
    public int insertDataItem(DataItem newData) {
        elementCounter++;
        String newKey = newData.dData;

        for(int i = (elementCounter - 1); i >= 0; i--) {
            System.out.println(i);
            if(dItemArray[i] == null) {
                continue;
            } else {
                String itsKey = dItemArray[i].dData;

                itsKey = dItemArray[i].dData;
                if (stringCompare(newKey, itsKey).equals(itsKey)) {
                    dItemArray[i + 1] = dItemArray[i];
                } else {
                    dItemArray[i + 1] = newData;
                    return i + 1;
                }
            }

        }
        dItemArray[0] = newData;

        return 0;
    }

    /**
     * Entfernt ein Datenelement aus dem Knoten
     * @return Das entfernte Datenelement
     * @throws NodeEmptyException
     */
    public DataItem removeDataItem()
            throws NodeEmptyException {
        //Prüfe, ob der Knoten noch leer ist
        if(isEmpty()) {
            throw new NodeEmptyException();
        }

        DataItem temp = dItemArray[elementCounter - 1];
        dItemArray[elementCounter - 1] = null;
        elementCounter --;
        return temp;
    }

    /**
     * Prüft, ob der Knoten ein Blatt ist
     * @return true falls ja; ansonsten false
     */
    public boolean isLeaf() {
        return (childArray[0] == null);
    }



    /**
     * Prüft, ob der Knoten voll ist
     * @return true falls er voll ist, ansonsten false
     */
    public boolean isFull() {
        return (elementCounter == MAX);
    }

    /**
     * Prüft, ob der Knoten leer ist
     * @return true falls er leer ist, ansonsten false
     */
    public boolean isEmpty() {
        return (elementCounter == 0);
    }


    /**
     * Vergleicht 2 Strings auf Größe
     * @param firstString   erster zu vergleichender String
     * @param secondString  zweiter zu vergleichender String
     * @return  der längere der beiden Strings
     */
    public String stringCompare(String firstString, String secondString) {
        int firstLength = firstString.length();
        int secondLength = secondString.length();
        int i = 0;
        while(i <= lengthCompare(firstLength, secondLength)) {
            if(firstString.charAt(i) > secondString.charAt(i)) {
                return firstString;
            } else if(firstString.charAt(i) < secondString.charAt(i)) {
                return secondString;
            } else {
                i++;
            }

        }
        return firstString;
    }


    /**
     * Vergleicht 2 String-Längen
     * @param keyLength     erste Länge eines Strings
     * @param newKeyLength  zweite Länge eines Strings
     * @return              die kleinere der beiden Längen
     */
    private int lengthCompare(int keyLength, int newKeyLength) {
        if(keyLength < newKeyLength) {
            return keyLength;
        }
        return newKeyLength;
    }

    public String toString() {
        String s = "{";
        for(int i = 0; i < getElementCounter(); i++) {
            s += dItemArray[i + 1];
            s += "/";
        }
        s += "}";
        return s;
    }

    public Node234 getChild(int childPos) {
        return childArray[childPos];
    }

    public Node234 getParent() {
        return parent;
    }

    public int getElementCounter() {
        return elementCounter;
    }

    public DataItem getDataItem(int index) {
        return dItemArray[index];
    }

}
