/**
 * Created by Andreas on 05.07.2016.
 */
public class IsLeafException extends Exception {

    private static final String IS_LEAF_MSG = "Der Knoten ist ein Blatt!";

    public IsLeafException() {
        super(IS_LEAF_MSG);
    }
}
