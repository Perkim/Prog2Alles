
/**
 * Liest eine Datei aus und erstellt je nach Inhalt der Datei einen 2-3-4 Baum
 * 
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan
 * @version 040716
 */
import java.io.*;

public class Read
{
    private Tree234 tree;
    
    
    private static final String INVALID_LINE_MSG        = "Ungültiger Parameter; Das erste Zeichen muss ein '+' oder ein '-' sein; Zeile wird übersprungen.";

    /**
     * Startet die Funktion zum Einlesen
     * @param arg Name des einzulesenden File
     * @throws FileDoesntExistException
     * @throws FileNotReadableException
     * @throws NoSimpleFileException
     * @throws IOException
     */
    public Read(String arg)
    throws FileDoesntExistException, FileNotReadableException, NoSimpleFileException, IOException{
        readFromPath(arg);
    }

    /**
     * Liest eine Datei ein
     * @param arg Name der einzulesenden Datei
     * @throws FileDoesntExistException
     * @throws FileNotReadableException
     * @throws NoSimpleFileException
     * @throws IOException
     */
    public void readFromPath(String arg)
    throws FileDoesntExistException, FileNotReadableException, NoSimpleFileException, IOException{
        FileReader fr = null;
        LineNumberReader lnr = null; 
        String str;
        
        File f = new File(arg);
        //Prüfe Datei auf Existenz
        if(!doesFileExist(f)) {
            throw new FileDoesntExistException();
        }
        //Prüfe Datei auf Lesbarkeit
        if(!isFileReadable(f)) {
            throw new FileNotReadableException(); 
        }
        //Prüfe Datei auf Directory
        if(isDirectory(f)) {
            throw new NoSimpleFileException(); 
        }
        
        try {
            //Erstelle neuen FileReader
            fr = new FileReader(f);
            lnr = new LineNumberReader(fr);
            
            int lineCounter = 1;
            tree = new Tree234();

            while((str = lnr.readLine()) != null) {
                //Ungültige Zeilen werden übersprungen
                System.out.println("Zeile: "  + lineCounter + ";");
            
                if((str.length() <= 1) || (str == null) || (str.equals("") || (str.equals(" ") ))) {
                    System.out.println(INVALID_LINE_MSG);
                    System.out.println("----------------------------------------------");
                    continue; 
                }
                
                //Wenn erstes Zeichen '+' ist, wird das '+' entfernt und der Name dem Baum hinzugefügt
                if(((str.charAt(0) == '+') && (str.length() > 1))) {
                    str = str.replace('+', ' ');

                    System.out.println("Insert: " + str);
                    tree.insert(str);
                    tree.displayTree();
                    lineCounter++;
                }
            }
        } catch(Exception e) {
            e.printStackTrace(); 
        } finally {
            //Beende Reader
            if(fr != null) {
                fr.close(); 
            }
            if(lnr != null) {
                lnr.close(); 
            }
        }
    }

    /**
     * Prüft, ob ein File existiert
     * @param f das zu prüfende File
     * @return true falls File existiert; ansonsten false
     */
    private boolean doesFileExist(File f) {
        if(f.exists()) {
            return true; 
        }
        return false;
    }

    /**
     * Prüft, ob ein File lesbar ist
     * @param f das zu prüfende File
     * @return true falls lesbar; ansonsten false
     */
    private boolean isFileReadable(File f) {
        if(f.canRead()) {
            return true;
        }
        return false; 
    }


    /**
     * Prüft, ob ein File ein Directory ist
     * @param f das zu prüfende Fil
     * @return true, falls File Directory ist, ansonsten false
     */
    private boolean isDirectory(File f) {
        if(f.isDirectory()) {
            return true; 
        }
        return false;
    }
}
