/**
 * Created by Andreas on 05.07.2016.
 */
public class NodeFullException extends Exception {

    private static final String NODE_IS_FULL_MSG = "Der Knoten ist voll";

    public NodeFullException() {
        super(NODE_IS_FULL_MSG);
    }
}
