
/**
 * Exception-Klasse, falls ein Knoten anhand des Schlüssels nicht gefunden werden konnte
 * 
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan
 * @version 130616
 */
public class NodeNotFoundException extends Exception
{
    private static final String NOTE_NOT_FOUND_MSG = "Es wurde kein Knoten mit dem übergebenen Schlüssel gefunden!";
    
    public NodeNotFoundException() {
        super(NOTE_NOT_FOUND_MSG); 
    }
}
