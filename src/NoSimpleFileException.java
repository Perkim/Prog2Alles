
/**
 * Exception-Klasse, falls die Datei kein einfaches File ist
 * 
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan 
 * @version 130616
 */
public class NoSimpleFileException extends Exception
{
    private static final String NO_SIMPLE_FILE_MSG = "Die eingegebene Datei ist keine einfache Datei; Bitte einfache Datei eingeben!";
    
    public NoSimpleFileException() {
        super(NO_SIMPLE_FILE_MSG); 
    }
}
