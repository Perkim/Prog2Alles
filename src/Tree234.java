
/**
 * Ein Baum
 * 
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan 
 * @version 040716
 */

import java.io.*;

public class Tree234
{
    private Node234 root;
    public Node234 node;
    
    public Tree234() {
        root = new Node234(); 
    }

    /**
     * Durchsucht den Baum nach einem Knoten
     * @param key Name des Knoten
     * @return Die Stelle des Knoten
     * @throws NodeEmptyException
     * @throws IsLeafException
     * @throws NodeFullException
     */
    public int findNode(String key)
    throws NodeEmptyException, IsLeafException, NodeFullException{
        Node234 currentNode = root;
        int childNumber;
        while(true) {
            if((childNumber = currentNode.findItem(key)) != -1) {
                return childNumber;
            } else if(currentNode.isLeaf()) {
                return -1; 
            } else {
                currentNode = getNextChild(currentNode, key);
            }
        }
    }

    /**
     * Fügt einen neuen Knoten in den Baum ein
     * @param value Das Datenelement des neuen Knoten
     * @throws NodeEmptyException
     * @throws NoPositionException
     * @throws PositionOccupiedException
     * @throws IsLeafException
     * @throws NodeFullException
     */
    public void insert(String value)
    throws NodeEmptyException, NoPositionException, PositionOccupiedException, IsLeafException, NodeFullException{
        Node234 current = root;
        DataItem temp = new DataItem(value);
        
        while(true) {
            if(current.isFull()) {
                split(current);
                current = current.getParent();
                current = getNextChild(current, value);
            } else if(current.isLeaf()) {
                break;
            } else {
                current = getNextChild(current, value);
            }
        }
        current.insertDataItem(temp); 
    }


    /**
     * Splittet einen Knoten auf
     * @param theNode der zu splittende Knoten
     * @throws NodeEmptyException
     * @throws NoPositionException
     * @throws PositionOccupiedException
     */
    public void split(Node234 theNode)
    throws NodeEmptyException, NoPositionException, PositionOccupiedException {
        DataItem dataB;
        DataItem dataC;
        
        Node234 parent;
        Node234 child2;
        Node234 child3;
        int itemIndex; 
        
        dataC = theNode.removeDataItem();
        dataB = theNode.removeDataItem();
        child2 = theNode.disconnectChildFromNode(2);
        child3 = theNode.disconnectChildFromNode(3);
        
        Node234 newRight = new Node234();
        
        if(node == root) {
            root = new Node234();
            parent = root;
            root.connectChildToNode(0, node);
        } else {
            parent = node.getParent(); 
        }
        
        itemIndex = parent.insertDataItem(dataB);
        int n = parent.getElementCounter();
        for(int j = n - 1; j > itemIndex; j--) {
            Node234 temp = parent.disconnectChildFromNode(j);
            parent.connectChildToNode(j + 1, temp);
        }
        parent.connectChildToNode(itemIndex + 1, newRight);
        
        newRight.insertDataItem(dataC);
        newRight.connectChildToNode(0, child2);
        newRight.connectChildToNode(1, child3); 
    }

    /**
     * Sucht den nächsten Kindsknoten eines übergebenen Knoten
     * @param theNode Knoten, dessen Kind gesucht wird
     * @param value Der Wert des Knoten
     * @return Der nächste Kindsknoten des Knoten
     * @throws IsLeafException
     * @throws NodeEmptyException
     * @throws NodeFullException
     */
    public Node234 getNextChild(Node234 theNode, String value)
    throws IsLeafException, NodeEmptyException, NodeFullException{
        //Exceptions, falls Knoten Blatt, voll oder leer
        if(theNode.isLeaf()) {
            throw new IsLeafException();
        }
        if(theNode.isEmpty()) {
            throw new NodeEmptyException();
        }
        if(theNode.isFull()) {
            throw new NodeFullException();
        }

        int i;
        int itemCounter = theNode.getElementCounter();

        for(i = 0; i < itemCounter; i++) {
            if(theNode.stringCompare(value, theNode.getDataItem(i).dData).equals(theNode.getDataItem(i).dData)) {
                return theNode.getChild(i);
            }
        }
        return theNode.getChild(i);
    }

    /**
     * Ruft die Funktion zur Darstellung des Baums auf; Beginnend bei der Wurzel
     */
    public void displayTree() {
        recDisplayTree(root, 0, 0); 
    }

    /**
     * Stellt den Baum Ebene für Ebene rekursiv dar
     * @param theNode Knoten der momentanen Ebene
     * @param level die momentane Ebene
     * @param childs die Anzahl der Kinder
     */
    private void recDisplayTree(Node234 theNode, int level, int childs) {
        System.out.print("Ebene: " + level + "; Kind: " + childs + "; ");
        System.out.println(theNode.toString());
        
        int itemCounter = theNode.getElementCounter();
        for(int i = 0; i < itemCounter + 1; i++) {
            Node234 nextNode = theNode.getChild(i);
            if(nextNode != null) {
                recDisplayTree(nextNode, level + 1, i);
            } else {
                return; 
            }
        }
    }
}
