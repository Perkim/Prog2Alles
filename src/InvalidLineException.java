
/**
 * Exception-Klasse, falls eine ungültige Zeile vorhanden ist
 * 
 * @author Andreas Bonny, Dennis Weisenseel, Klaus Luhan 
 * @version 130616
 */
public class InvalidLineException extends Exception
{
    private static final String INVALID_LINE_MSG    = "Ungültiger Parameter; Das erste Zeichen muss ein '+' oder ein '-' sein; Zeile wird übersprungen.";
    
    public InvalidLineException() {
        super(INVALID_LINE_MSG);
    }
}
