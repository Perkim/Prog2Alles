/**
 * Created by Andreas on 04.07.2016.
 */

public class NodeEmptyException extends Exception {

    private static final String NODE_EMPTY_MSG = "Der Knoten enthält noch keine Daten!";

    public NodeEmptyException() {
        super(NODE_EMPTY_MSG);
    }
}
